import * as actions from './actions';

export const initialState = {
    categories: [],
    error: null,
    items: [],
    loading: false,
    modalOpen: false,
};

export const reducer = (state, action) => {
    switch (action.type) {
        case actions.IMAGES_SET_LOADING:
            return {
                ...state,
                loading: action.value,
            };
        case actions.IMAGES_SET_ITEMS:
            return {
                ...state,
                items: action.items,
            };
        case actions.IMAGES_SET_ERROR:
            return {
                ...state,
                error: action.error,
            };
        case actions.IMAGES_SET_MODAL_OPEN:
            return {
                ...state,
                modalOpen: action.value,
            };
        case actions.IMAGES_SET_CATEGORIES:
            return {
                ...state,
                categories: action.categories,
            };
        case actions.IMAGES_CLEAR_STATE:
            return initialState;
        default:
            return state;
    }
};
