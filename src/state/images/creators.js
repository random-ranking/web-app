import * as actions from './actions';

export const setLoading = value => ({
    type: actions.IMAGES_SET_LOADING,
    value,
});

export const setItems = items => ({
    type: actions.IMAGES_SET_ITEMS,
    items,
});

export const setError = error => ({
    type: actions.IMAGES_SET_ERROR,
    error,
});

export const setModalOpen = value => ({
    type: actions.IMAGES_SET_MODAL_OPEN,
    value,
});

export const setCategories = categories => {
    categories.sort((a, b) => {
        if (a.description > b.description) {
            return 1;
        }
        if (b.description > a.description) {
            return -1;
        }

        return 0;
    });

    return {
        type: actions.IMAGES_SET_CATEGORIES,
        categories,
    };
};

export const clearState = () => ({
    type: actions.IMAGES_CLEAR_STATE,
});
