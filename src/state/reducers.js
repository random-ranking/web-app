import * as authState from './auth/reducer';
import * as commonState from './common/reducer';
import * as imagesState from './images/reducer';
import * as privateRoomsState from './privateRooms/reducer';

export const initialState = {
    auth: authState.initialState,
    common: commonState.initialState,
    images: imagesState.initialState,
    privateRooms: privateRoomsState.initialState,
};

export const mainReducer = (
    { auth, common, images, privateRooms },
    action
) => ({
    auth: authState.reducer(auth, action),
    common: commonState.reducer(common, action),
    images: imagesState.reducer(images, action),
    privateRooms: privateRoomsState.reducer(privateRooms, action),
});
