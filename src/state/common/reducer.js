import * as actions from './actions';

export const initialState = {
    loading: false,
    serviceAvailable: true,
};

export const reducer = (state, action) => {
    switch (action.type) {
        case actions.COMMON_SET_LOADING:
            return {
                ...state,
                loading: action.value,
            };
        case actions.COMMON_SET_SERVICE_AVAILABLE:
            return {
                ...state,
                serviceAvailable: action.value,
            };
        default:
            return state;
    }
};
