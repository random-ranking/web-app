import * as actions from './actions';

export const setLoading = value => ({
    type: actions.COMMON_SET_LOADING,
    value,
});

export const setServiceAvailable = value => ({
    type: actions.COMMON_SET_SERVICE_AVAILABLE,
    value,
});
