import * as actions from './actions';

export const initialState = {
    error: null,
    items: [],
    loading: false,
    modalOpen: false,
};

export const reducer = (state, action) => {
    switch (action.type) {
        case actions.PRIVATE_ROOMS_SET_LOADING:
            return {
                ...state,
                loading: action.value,
            };
        case actions.PRIVATE_ROOMS_SET_ITEMS:
            return {
                ...state,
                items: action.items,
            };
        case actions.PRIVATE_ROOMS_SET_ERROR:
            return {
                ...state,
                error: action.error,
            };
        case actions.PRIVATE_ROOMS_SET_MODAL_OPEN:
            return {
                ...state,
                modalOpen: action.value,
            };
        case actions.PRIVATE_ROOMS_CLEAR_STATE:
            return initialState;
        default:
            return state;
    }
};
