import * as actions from './actions';

export const setLoading = value => ({
    type: actions.PRIVATE_ROOMS_SET_LOADING,
    value,
});

export const setItems = items => ({
    type: actions.PRIVATE_ROOMS_SET_ITEMS,
    items,
});

export const setError = error => ({
    type: actions.PRIVATE_ROOMS_SET_ERROR,
    error,
});

export const setModalOpen = value => ({
    type: actions.PRIVATE_ROOMS_SET_MODAL_OPEN,
    value,
});

export const clearState = () => ({
    type: actions.PRIVATE_ROOMS_CLEAR_STATE,
});
