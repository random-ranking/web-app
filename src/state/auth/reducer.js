import * as actions from './actions';

export const initialState = {
    authenticated: false,
    currentUser: {
        uid: '',
        claims: null,
        privateRoomCode: '',
    },
    error: null,
};

export const reducer = (state, action) => {
    switch (action.type) {
        case actions.AUTH_SET_AUTHENTICATED:
            return {
                ...state,
                authenticated: action.payload.authenticated,
                currentUser: {
                    uid: action.payload.uid,
                    claims: action.payload.claims,
                    privateRoomCode: action.payload.privateRoomCode,
                },
            };
        case actions.AUTH_SET_ERROR:
            return {
                ...state,
                error: action.error,
            };
        default:
            return state;
    }
};
