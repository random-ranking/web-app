import * as actions from './actions';

export const setAuthenticated = (
    authenticated,
    uid = '',
    claims = null,
    privateRoomCode = ''
) => ({
    type: actions.AUTH_SET_AUTHENTICATED,
    payload: {
        authenticated,
        uid,
        claims,
        privateRoomCode,
    },
});

export const setError = error => ({ type: actions.AUTH_SET_ERROR, error });
