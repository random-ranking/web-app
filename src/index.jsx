import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { SnackbarProvider } from 'notistack';

import * as state from './state';
import * as mainState from './state/reducers';
import theme from './theme';
import constants from './config/constants';

import App from './components/App';

ReactDOM.render(
    <>
        <ThemeProvider theme={theme}>
            <SnackbarProvider
                maxSnack={constants.snackbar.MAX_SNACK}
                anchorOrigin={{
                    vertical: constants.snackbar.ANCHOR_ORIGIN_VERTICAL,
                    horizontal: constants.snackbar.ANCHOR_ORIGIN_HORIZONTAL,
                }}
            >
                <state.StateProvider
                    initialState={mainState.initialState}
                    reducer={mainState.mainReducer}
                >
                    <App />
                </state.StateProvider>
            </SnackbarProvider>
        </ThemeProvider>
    </>,
    document.getElementById('root')
);
