import React, { useEffect } from 'react';
import { useSnackbar } from 'notistack';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Grid from '@material-ui/core/Grid';

import TransitionsModal from '../TransitionsModal';
import ImageForm from '../ImageForm';
import ImageCard from '../ImageCard';
import Illustration from '../Illustration';

import services from '../../services';
import { useStateValue } from '../../state';
import * as imagesCreators from '../../state/images/creators';
import * as commonCreators from '../../state/common/creators';
import messages from '../../config/messages';
import constants from '../../config/constants';
import NotFoundImage from '../../resources/images/not_found.svg';

const useStyles = makeStyles(theme => ({
    button: {
        marginLeft: theme.spacing(2),
    },
    grid: {
        marginTop: theme.spacing(4),
    },
    header: {
        display: 'inline-flex',
        alignItems: 'center',
    },
}));

const ImagesUploadPage = () => {
    const classes = useStyles();
    const [{ auth, images }, dispatch] = useStateValue();
    const { enqueueSnackbar } = useSnackbar();

    const setError = err => {
        dispatch(imagesCreators.setError(err));
    };

    useEffect(() => {
        dispatch(commonCreators.setLoading(true));

        const getCategories = async () => {
            try {
                const data = await services.api.getCategories();

                dispatch(imagesCreators.setCategories(data.categories));
            } catch (err) {
                setError(err);
            }
        };
        getCategories();

        const unsubscribe = services.api.listenerImages(listenerImages => {
            dispatch(commonCreators.setLoading(true));

            dispatch(imagesCreators.setItems(listenerImages));

            dispatch(commonCreators.setLoading(false));
        }, auth.currentUser.privateRoomCode);

        return () => {
            unsubscribe();
        };
        // eslint-disable-next-line
    }, []);

    const handleOpen = () => {
        dispatch(imagesCreators.setModalOpen(true));
    };

    const handleClose = () => {
        if (!images.loading) {
            dispatch(imagesCreators.setModalOpen(false));
        }
    };

    const handleSubmit = async data => {
        dispatch(imagesCreators.setLoading(true));

        try {
            await services.api.createImage(
                data.category,
                data.image,
                auth.currentUser.privateRoomCode
            );

            dispatch(imagesCreators.setModalOpen(false));
            enqueueSnackbar(messages.imagesUploadPage.notifications.SUCCESS, {
                variant: 'success',
            });
        } catch (err) {
            setError(err);
        } finally {
            dispatch(imagesCreators.setLoading(false));
        }
    };

    const handleDeleteImage = async imageId => {
        dispatch(commonCreators.setLoading(true));

        try {
            await services.api.deleteImage(
                imageId,
                auth.currentUser.privateRoomCode
            );

            enqueueSnackbar(messages.imagesUploadPage.notifications.DELETE, {
                variant: 'success',
            });
        } catch (err) {
            setError(err);
        } finally {
            dispatch(commonCreators.setLoading(false));
        }
    };

    return (
        <>
            <div className={classes.header}>
                <Typography variant='h4'>
                    {auth.currentUser.claims[constants.roles.PRIVATE_ROOM_OWNER]
                        ? messages.imagesUploadPage.TITLE_CLIENT
                        : messages.imagesUploadPage.TITLE}
                </Typography>
                <Button
                    variant='contained'
                    color='primary'
                    size='medium'
                    className={classes.button}
                    startIcon={<CloudUploadIcon />}
                    aria-label={messages.imagesUploadPage.ARIA_LABEL_NEW_IMAGE}
                    onClick={handleOpen}
                >
                    {messages.imagesUploadPage.BUTTON}
                </Button>
            </div>
            {images.items.length ? (
                <Grid className={classes.grid} container spacing={2}>
                    {images.items.map(i => (
                        <Grid key={i.id} item md={4} xs={12}>
                            <ImageCard
                                deleteText={
                                    messages.imagesUploadPage.card.DELETE_BUTTON
                                }
                                handleDelete={handleDeleteImage}
                                image={i}
                            />
                        </Grid>
                    ))}
                </Grid>
            ) : (
                <Illustration
                    alt={messages.imagesUploadPage.illustration.ALT}
                    message={messages.imagesUploadPage.illustration.MESSAGE}
                    src={NotFoundImage}
                    width={500}
                />
            )}
            <TransitionsModal handleClose={handleClose} open={images.modalOpen}>
                <ImageForm
                    categories={images.categories}
                    cleanError={() => setError(null)}
                    handleClose={handleClose}
                    handleSubmit={handleSubmit}
                    hasError={!!images.error}
                    loading={images.loading}
                />
            </TransitionsModal>
        </>
    );
};

export default ImagesUploadPage;
