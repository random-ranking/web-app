import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

import { useStateValue } from '../../state';

import Routes from '../Routes';

const useStyles = makeStyles(theme => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}));

const App = () => {
    const classes = useStyles();
    const [{ common }] = useStateValue();

    return (
        <>
            <Routes />
            <Backdrop className={classes.backdrop} open={common.loading}>
                <CircularProgress color='inherit' />
            </Backdrop>
        </>
    );
};

export default App;
