import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PhotoLibraryRoundedIcon from '@material-ui/icons/PhotoLibraryRounded';
import MeetingRoomRoundedIcon from '@material-ui/icons/MeetingRoomRounded';
import LockIcon from '@material-ui/icons/Lock';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

import routes from '../../config/routes';
import messages from '../../config/messages';
import services from '../../services';
import { useStateValue } from '../../state';
import * as authCreators from '../../state/auth/creators';
import * as imagesCreators from '../../state/images/creators';
import * as privateRoomsCreators from '../../state/privateRooms/creators';
import * as commonCreators from '../../state/common/creators';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    list: {
        height: 'inherit',
    },
    bottomItem: {
        bottom: '0px',
        marginBottom: theme.spacing(2),
        position: 'absolute',
    },
    chip: {
        width: '100%',
        margin: theme.spacing(2),
    },
}));

const menus = [
    {
        icon: PhotoLibraryRoundedIcon,
        route: routes.images_upload.ROUTE,
        text: messages.routes.IMAGES_UPLOAD,
        authorizedRoles: routes.images_upload.AUTHORIZED_ROLES,
    },
    {
        icon: MeetingRoomRoundedIcon,
        route: routes.private_rooms.ROUTE,
        text: messages.routes.PRIVATE_ROOMS,
        authorizedRoles: routes.private_rooms.AUTHORIZED_ROLES,
    },
];

const Main = ({ component: Component, history }) => {
    const classes = useStyles();
    const [
        {
            auth: { currentUser },
        },
        dispatch,
    ] = useStateValue();

    const handleLogOut = async () => {
        dispatch(commonCreators.setLoading(true));

        try {
            await services.api.doSignOut();
        } catch (err) {
            dispatch(authCreators.setError(err));
        } finally {
            dispatch(imagesCreators.clearState());
            dispatch(privateRoomsCreators.clearState());
            dispatch(authCreators.setAuthenticated(false));
            dispatch(commonCreators.setLoading(false));

            history.push(routes.sign_in.ROUTE);
        }
    };

    return (
        <div className={classes.root}>
            <Drawer
                className={classes.drawer}
                variant='permanent'
                classes={{
                    paper: classes.drawerPaper,
                }}
                anchor='left'
            >
                <div className={classes.toolbar} />
                <Divider />
                {currentUser.privateRoomCode && (
                    <>
                        <ListItem disabled>
                            <ListItemIcon>
                                <LockIcon />
                            </ListItemIcon>
                            <ListItemText
                                primary={`${messages.main.PRIVATE_ROOM_CODE} ${currentUser.privateRoomCode}`}
                            />
                        </ListItem>
                        <Divider />
                    </>
                )}
                <List className={classes.list}>
                    {menus.map(m =>
                        !m.authorizedRoles.length ||
                        m.authorizedRoles.some(rq => currentUser.claims[rq]) ? (
                            <ListItem
                                key={m.text}
                                button
                                onClick={() => history.push(m.route)}
                                selected={history.location.pathname === m.route}
                                disabled={history.location.pathname === m.route}
                            >
                                <ListItemIcon>
                                    <m.icon />
                                </ListItemIcon>
                                <ListItemText primary={m.text} />
                            </ListItem>
                        ) : null
                    )}
                    <ListItem className={classes.bottomItem}>
                        <Button
                            fullWidth
                            variant='contained'
                            color='primary'
                            onClick={handleLogOut}
                        >
                            {messages.routes.LOG_OUT}
                        </Button>
                    </ListItem>
                </List>
            </Drawer>
            <main className={classes.content}>
                <Component />
            </main>
        </div>
    );
};

Main.propTypes = {
    component: PropTypes.func.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
        location: PropTypes.object.isRequired,
    }).isRequired,
};

export default withRouter(Main);
