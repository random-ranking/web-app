import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    container: {
        marginLeft: theme.spacing(2),
        display: 'flex',
        width: 'fit-content',
        border: `1px solid ${theme.palette.divider}`,
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.background.paper,
    },
    information: {
        padding: theme.spacing(1),
    },
    title: {
        padding: theme.spacing(1),
        backgroundColor: theme.palette.grey[200],
    },
}));

const InfoWithDivider = ({ information, title }) => {
    const classes = useStyles();

    return (
        <Grid className={classes.container}>
            <Typography className={classes.title}>{title}</Typography>
            <Divider orientation='vertical' flexItem />
            <Typography className={classes.information}>
                {information}
            </Typography>
        </Grid>
    );
};

InfoWithDivider.propTypes = {
    information: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

export default InfoWithDivider;
