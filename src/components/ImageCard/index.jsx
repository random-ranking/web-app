import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import FavoriteIcon from '@material-ui/icons/Favorite';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 275,
    },
    icons: {
        display: 'flex',
    },
    likes: {
        display: 'inline-flex',
    },
    dislikes: {
        display: 'inline-flex',
        marginLeft: 'auto',
    },
    imageContainer: {
        position: 'relative',
    },
    category: {
        position: 'absolute',
        top: theme.spacing(1),
        right: 0,
        margin: theme.spacing(0, 1, 1, 0),
    },
    image: {
        objectFit: 'fill',
        height: 250,
    },
    iconInfo: {
        marginLeft: theme.spacing(1),
    },
    favoriteIcon: {
        color: '#f44336',
    },
    notInterestedIcon: {
        marginLeft: theme.spacing(1),
    },
}));

const ImageCard = ({ deleteText, handleDelete, image }) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <div className={classes.imageContainer}>
                <Chip
                    className={classes.category}
                    label={image.category}
                    color='secondary'
                />
                <CardMedia
                    src={image.image}
                    className={classes.image}
                    alt='image'
                    component='img'
                />
            </div>
            <CardContent className={classes.icons}>
                <div className={classes.likes}>
                    <FavoriteIcon className={classes.favoriteIcon} />
                    <Typography className={classes.iconInfo}>
                        {image.likes.length}
                    </Typography>
                    <NotInterestedIcon className={classes.notInterestedIcon} />
                    <Typography className={classes.iconInfo}>
                        {image.dislikes.length}
                    </Typography>
                </div>
                <div className={classes.dislikes}>
                    <Button
                        variant='contained'
                        color='primary'
                        size='medium'
                        className={classes.button}
                        startIcon={<DeleteIcon />}
                        aria-label={deleteText}
                        onClick={() => handleDelete(image.id)}
                    >
                        {deleteText}
                    </Button>
                </div>
            </CardContent>
        </Card>
    );
};

ImageCard.propTypes = {
    deleteText: PropTypes.string.isRequired,
    handleDelete: PropTypes.func.isRequired,
    image: PropTypes.shape({
        category: PropTypes.string.isRequired,
        dislikes: PropTypes.arrayOf(PropTypes.string).isRequired,
        id: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
        likes: PropTypes.arrayOf(PropTypes.string).isRequired,
    }).isRequired,
};

export default ImageCard;
