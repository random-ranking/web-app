import React, { useEffect } from 'react';
import { useSnackbar } from 'notistack';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import TransitionsModal from '../TransitionsModal';
import PrivateRoomForm from '../PrivateRoomForm';
import PrivateRoomCard from '../PrivateRoomCard';
import Illustration from '../Illustration';

import services from '../../services';
import { useStateValue } from '../../state';
import * as privateRoomsCreators from '../../state/privateRooms/creators';
import * as commonCreators from '../../state/common/creators';
import messages from '../../config/messages';
import NotFoundImage from '../../resources/images/not_found.svg';

const useStyles = makeStyles(theme => ({
    button: {
        marginLeft: theme.spacing(2),
    },
    grid: {
        marginTop: theme.spacing(4),
    },
    header: {
        display: 'inline-flex',
        alignItems: 'center',
    },
}));

const PrivateRoomsPage = () => {
    const classes = useStyles();
    const [{ privateRooms }, dispatch] = useStateValue();
    const { enqueueSnackbar } = useSnackbar();

    const setError = err => {
        dispatch(privateRoomsCreators.setError(err));
    };

    useEffect(() => {
        dispatch(commonCreators.setLoading(true));

        const unsubscribe = services.api.listenerPrivateRooms(
            listenerPrivateRooms => {
                dispatch(commonCreators.setLoading(true));

                dispatch(privateRoomsCreators.setItems(listenerPrivateRooms));

                dispatch(commonCreators.setLoading(false));
            }
        );

        return () => {
            unsubscribe();
        };
        // eslint-disable-next-line
    }, []);

    const handleOpen = () => {
        dispatch(privateRoomsCreators.setModalOpen(true));
    };

    const handleClose = () => {
        if (!privateRooms.loading) {
            dispatch(privateRoomsCreators.setModalOpen(false));
        }
    };

    const handleSubmit = async data => {
        dispatch(privateRoomsCreators.setLoading(true));

        try {
            const createUserData = await services.api.createUser(
                data.email,
                data.password
            );
            const privateRoomData = await services.api.createPrivateRoom(
                createUserData.uid
            );

            dispatch(privateRoomsCreators.setModalOpen(false));
            enqueueSnackbar(
                `${messages.privateRoomsPage.notifications.SUCCESS} ${privateRoomData.code}`,
                {
                    variant: 'success',
                }
            );
        } catch (err) {
            setError(err);
        } finally {
            dispatch(privateRoomsCreators.setLoading(false));
        }
    };

    return (
        <>
            <div className={classes.header}>
                <Typography variant='h4'>
                    {messages.privateRoomsPage.TITLE}
                </Typography>
                <Button
                    variant='contained'
                    color='primary'
                    size='medium'
                    className={classes.button}
                    startIcon={<CloudUploadIcon />}
                    aria-label={messages.privateRoomsPage.ARIA_LABEL_NEW_IMAGE}
                    onClick={handleOpen}
                >
                    {messages.privateRoomsPage.BUTTON}
                </Button>
            </div>
            {privateRooms.items.length ? (
                <Grid className={classes.grid} container spacing={2}>
                    {privateRooms.items.map(i => (
                        <Grid key={i.code} item md={12}>
                            <PrivateRoomCard privateRoom={i} />
                        </Grid>
                    ))}
                </Grid>
            ) : (
                <Illustration
                    alt={messages.privateRoomsPage.illustration.ALT}
                    message={messages.privateRoomsPage.illustration.MESSAGE}
                    src={NotFoundImage}
                    width={500}
                />
            )}
            <TransitionsModal
                handleClose={handleClose}
                open={privateRooms.modalOpen}
            >
                <PrivateRoomForm
                    cleanError={() => setError(null)}
                    handleClose={handleClose}
                    handleSubmit={handleSubmit}
                    hasError={!!privateRooms.error}
                    loading={privateRooms.loading}
                />
            </TransitionsModal>
        </>
    );
};

export default PrivateRoomsPage;
