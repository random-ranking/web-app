import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Skeleton from '@material-ui/lab/Skeleton';
import PhotoIcon from '@material-ui/icons/Photo';

import messages from '../../config/messages';

const useStyles = makeStyles(theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(5),
        width: 350,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    submit: {
        marginTop: theme.spacing(3),
    },
    close: {
        marginBottom: theme.spacing(2),
    },
    error: {
        textAlign: 'center',
        color: theme.palette.error.main,
    },
    fileInputContainer: {
        margin: theme.spacing(2, 0, 2, 0),
        display: 'flex',
        alignItems: 'baseline',
    },
    fileInput: {
        display: 'none',
    },
    fileName: {
        marginLeft: theme.spacing(1),
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        width: '100%',
    },
    select: {
        margin: theme.spacing(2, 0, 2, 0),
        width: '100%',
    },
}));

const ImageForm = ({
    categories,
    cleanError,
    handleClose,
    handleSubmit,
    hasError,
    loading,
}) => {
    const classes = useStyles();
    const [inputs, setInputs] = useState({
        category: categories.length ? categories[0].description : '',
        image: null,
    });

    const handleOnChange = event => {
        event.preventDefault();

        cleanError();

        setInputs({
            ...inputs,
            [event.target.name]: event.target.value,
        });
    };

    const handleOnChangeFile = event => {
        event.preventDefault();

        cleanError();

        setInputs({
            ...inputs,
            image: event.target.files[0],
        });
    };

    const ownHanldeSubmit = async () => handleSubmit(inputs);

    return (
        <Paper className={classes.paper} elevation={3}>
            <Typography className={classes.title} component='h1' variant='h5'>
                {messages.imageForm.TITLE}
            </Typography>
            <form className={classes.form} noValidate>
                <InputLabel>{messages.imageForm.form.IMAGE_LABEL}</InputLabel>
                <div className={classes.fileInputContainer}>
                    <InputLabel>
                        <IconButton
                            color='primary'
                            aria-label='upload picture'
                            component='span'
                        >
                            <input
                                accept='image/*'
                                className={classes.fileInput}
                                type='file'
                                onChange={handleOnChangeFile}
                                disabled={loading}
                            />
                            <PhotoIcon />
                        </IconButton>
                    </InputLabel>
                    {inputs.image ? (
                        <Typography className={classes.fileName}>
                            {inputs.image.name}
                        </Typography>
                    ) : (
                        <Skeleton
                            className={classes.fileName}
                            animation={false}
                            variant='text'
                        />
                    )}
                </div>
                <InputLabel id='category-select-label'>
                    {messages.imageForm.form.CATEGORY_LABEL}
                </InputLabel>
                <Select
                    labelId='category-select-label'
                    value={inputs.category}
                    name='category'
                    className={classes.select}
                    onChange={handleOnChange}
                    disabled={loading}
                >
                    {categories.map(c => (
                        <MenuItem key={c.id} value={c.description}>
                            {c.description}
                        </MenuItem>
                    ))}
                </Select>
                <Button
                    type='button'
                    fullWidth
                    variant='contained'
                    color='primary'
                    className={classes.submit}
                    onClick={ownHanldeSubmit}
                    disabled={
                        hasError || loading || !inputs.image || !inputs.category
                    }
                >
                    {messages.imageForm.form.BUTTON}
                </Button>
                <Button
                    type='button'
                    fullWidth
                    variant='text'
                    color='inherit'
                    className={classes.submit}
                    onClick={handleClose}
                    disabled={loading}
                >
                    {messages.imageForm.form.CLOSE_BUTTON}
                </Button>
                {hasError && (
                    <Typography className={classes.error}>
                        {messages.imageForm.form.ERROR_MESSAGE}
                    </Typography>
                )}
            </form>
        </Paper>
    );
};

ImageForm.propTypes = {
    categories: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string.isRequired,
            description: PropTypes.string.isRequired,
        })
    ).isRequired,
    cleanError: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    hasError: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
};

export default ImageForm;
