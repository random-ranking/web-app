import React from 'react';

import Illustration from '../Illustration';

import messages from '../../config/messages';
import UnauthorizedImage from '../../resources/images/unauthorized.svg';

const UnauthorizedPage = () => (
    <>
        <Illustration
            alt={messages.unauthorizedPage.illustration.ALT}
            message={messages.unauthorizedPage.illustration.MESSAGE}
            src={UnauthorizedImage}
            width={500}
        />
    </>
);

export default UnauthorizedPage;
