import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    container: {
        alignItems: 'center',
        height: '80vh',
        justifyContent: 'center',
    },
    message: {
        color: theme.palette.grey[400],
        marginTop: theme.spacing(2),
    },
}));

const Illustration = ({ alt, message, src, width }) => {
    const classes = useStyles();

    return (
        <Grid
            container
            spacing={0}
            direction='column'
            className={classes.container}
        >
            <img alt={alt} src={src} width={width} />
            {message && (
                <Typography className={classes.message} variant='h5'>
                    {message}
                </Typography>
            )}
        </Grid>
    );
};

Illustration.defaultProps = {
    message: '',
};

Illustration.propTypes = {
    alt: PropTypes.string.isRequired,
    message: PropTypes.string,
    src: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
};

export default Illustration;
