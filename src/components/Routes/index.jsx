import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import { useStateValue } from '../../state';
import routes from '../../config/routes';

import SignIn from '../SignIn';
import ImagesUploadPage from '../ImagesUploadPage';
import PrivateRoomsPage from '../PrivateRoomsPage';
import PrivateRoute from '../PrivateRoute';
import UnauthorizedPage from '../UnauthorizedPage';

const Routes = () => {
    const [{ auth }] = useStateValue();

    return (
        <Router>
            <Route path={routes.sign_in.ROUTE} component={SignIn} />
            <PrivateRoute
                authenticated={auth.authenticated}
                authorizedRoles={routes.unauthorized.AUTHORIZED_ROLES}
                component={UnauthorizedPage}
                path={routes.unauthorized.ROUTE}
                userClaims={auth.currentUser.claims}
            />
            <PrivateRoute
                authenticated={auth.authenticated}
                authorizedRoles={routes.images_upload.AUTHORIZED_ROLES}
                component={ImagesUploadPage}
                path={routes.images_upload.ROUTE}
                userClaims={auth.currentUser.claims}
            />
            <PrivateRoute
                authenticated={auth.authenticated}
                authorizedRoles={routes.private_rooms.AUTHORIZED_ROLES}
                component={PrivateRoomsPage}
                path={routes.private_rooms.ROUTE}
                userClaims={auth.currentUser.claims}
            />

            <Redirect to={routes.images_upload.ROUTE} />
        </Router>
    );
};

export default Routes;
