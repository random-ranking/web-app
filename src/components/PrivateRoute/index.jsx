/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

import routes from '../../config/routes';

import Main from '../Main';

const PrivateRoute = ({
    authenticated,
    authorizedRoles,
    component,
    userClaims,
    ...rest
}) => {
    const userHasRequiredRole =
        !authorizedRoles.length ||
        (!!userClaims && authorizedRoles.some(ar => userClaims[ar]));

    const renderAuthenticated = ({ location, ...props }) => {
        if (authenticated) {
            if (userHasRequiredRole) {
                return (
                    <Main
                        component={component}
                        location={location}
                        {...props}
                    />
                );
            }

            return (
                <Redirect
                    to={{
                        pathname: routes.unauthorized.ROUTE,
                        state: { from: location },
                    }}
                />
            );
        }

        return (
            <Redirect
                to={{
                    pathname: routes.sign_in.ROUTE,
                    state: { from: location },
                }}
            />
        );
    };

    renderAuthenticated.propTypes = {
        location: PropTypes.string.isRequired,
    };

    return <Route {...rest} render={renderAuthenticated} />;
};

PrivateRoute.defaultProps = {
    userClaims: null,
};

PrivateRoute.propTypes = {
    authenticated: PropTypes.bool.isRequired,
    authorizedRoles: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,
    component: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    userClaims: PropTypes.object,
};

export default PrivateRoute;
