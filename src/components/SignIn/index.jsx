import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LinearProgress from '@material-ui/core/LinearProgress';

import services from '../../services';
import { useStateValue } from '../../state';
import * as authCreators from '../../state/auth/creators';
import * as commonCreators from '../../state/common/creators';
import messages from '../../config/messages';
import routes from '../../config/routes';
import constants from '../../config/constants';

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(15),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(5),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    error: {
        textAlign: 'center',
        color: theme.palette.error.main,
    },
}));

const SignIn = ({ history }) => {
    const classes = useStyles();
    const [{ auth }, dispatch] = useStateValue();
    const [inputs, setInputs] = useState({ email: '', password: '' });
    const [showChild, setShowChild] = useState(false);

    useEffect(() => {
        services.api.onAuthStateChanged(async (user, claims) => {
            if (user) {
                let code = '';

                if (claims[constants.roles.PRIVATE_ROOM_OWNER]) {
                    const data = await services.api.getPrivateRoomByOwnerId(
                        user.uid
                    );

                    code = data.privateRoom.code;
                }

                dispatch(authCreators.setError(null));
                dispatch(
                    authCreators.setAuthenticated(true, user.uid, claims, code)
                );

                history.push(routes.images_upload.ROUTE);
            }

            dispatch(commonCreators.setLoading(false));
            setShowChild(true);
        });
        // eslint-disable-next-line
    }, []);

    const handleOnChange = event => {
        event.preventDefault();

        dispatch(authCreators.setError(null));

        setInputs({
            ...inputs,
            [event.target.name]: event.target.value,
        });
    };

    const handleOnClick = async () => {
        dispatch(commonCreators.setLoading(true));

        try {
            await services.api.doSignInWithEmailAndPassword(
                inputs.email,
                inputs.password
            );
        } catch (err) {
            dispatch(authCreators.setError(err));
        }
    };

    return showChild ? (
        <Container component='main' maxWidth='xs'>
            <Paper className={classes.paper} elevation={3}>
                <Typography component='h1' variant='h5'>
                    {messages.signIn.TITLE}
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        margin='normal'
                        fullWidth
                        id='email'
                        label={messages.signIn.form.EMAIL_ADDRESS_LABEL}
                        name='email'
                        autoComplete='email'
                        autoFocus
                        onChange={handleOnChange}
                        value={inputs.email}
                    />
                    <TextField
                        margin='normal'
                        fullWidth
                        name='password'
                        label={messages.signIn.form.PASSWORD_LABEL}
                        type='password'
                        id='password'
                        autoComplete='current-password'
                        onChange={handleOnChange}
                        value={inputs.password}
                    />
                    <Button
                        type='button'
                        fullWidth
                        variant='contained'
                        color='primary'
                        className={classes.submit}
                        onClick={handleOnClick}
                        disabled={
                            !!auth.error || !inputs.email || !inputs.password
                        }
                    >
                        {messages.signIn.form.BUTTON}
                    </Button>
                    {auth.error && (
                        <Typography className={classes.error}>
                            {messages.signIn.form.ERROR_MESSAGE}
                        </Typography>
                    )}
                </form>
            </Paper>
        </Container>
    ) : (
        <LinearProgress />
    );
};

SignIn.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
};

export default withRouter(SignIn);
