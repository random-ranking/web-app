import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import InfoWithDivider from '../InfoWithDivider';

import messages from '../../config/messages';

const useStyles = makeStyles({
    root: {
        display: 'flex',
    },
});

const PrivateRoomCard = ({ privateRoom }) => {
    const classes = useStyles();

    return (
        <Card variant='outlined'>
            <CardContent className={classes.root}>
                <InfoWithDivider
                    information={privateRoom.code}
                    title={messages.privateRoomsPage.card.CODE_LABEL}
                />
                <InfoWithDivider
                    information={privateRoom.ownerEmail}
                    title={messages.privateRoomsPage.card.EMAIL_LABEL}
                />
                <InfoWithDivider
                    information={privateRoom.ownerId}
                    title={messages.privateRoomsPage.card.UID_LABEL}
                />
            </CardContent>
        </Card>
    );
};

PrivateRoomCard.propTypes = {
    privateRoom: PropTypes.shape({
        code: PropTypes.string.isRequired,
        ownerId: PropTypes.string.isRequired,
        ownerEmail: PropTypes.string.isRequired,
    }).isRequired,
};

export default PrivateRoomCard;
