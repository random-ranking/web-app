import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

import { TextField } from '@material-ui/core';
import messages from '../../config/messages';

const useStyles = makeStyles(theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(5),
        width: 350,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    submit: {
        marginTop: theme.spacing(3),
    },
    close: {
        marginBottom: theme.spacing(2),
    },
    error: {
        textAlign: 'center',
        color: theme.palette.error.main,
    },
    input: {
        marginBottom: theme.spacing(2),
        width: '100%',
    },
}));

const PrivateRoomForm = ({
    cleanError,
    handleClose,
    handleSubmit,
    hasError,
    loading,
}) => {
    const classes = useStyles();
    const [inputs, setInputs] = useState({ email: '', password: '' });

    const handleOnChange = event => {
        event.preventDefault();

        cleanError();

        setInputs({
            ...inputs,
            [event.target.name]: event.target.value,
        });
    };

    const ownHandleSubmit = async () => handleSubmit(inputs);

    return (
        <Paper className={classes.paper} elevation={3}>
            <Typography className={classes.title} component='h1' variant='h5'>
                {messages.privateRoomForm.TITLE}
            </Typography>
            <form className={classes.form} noValidate>
                <TextField
                    className={classes.input}
                    label={messages.privateRoomForm.form.EMAIL_LABEL}
                    value={inputs.email}
                    name='email'
                    onChange={handleOnChange}
                    disabled={loading}
                />
                <TextField
                    className={classes.input}
                    label={messages.privateRoomForm.form.PASSWORD_LABEL}
                    value={inputs.password}
                    name='password'
                    onChange={handleOnChange}
                    disabled={loading}
                />
                <Button
                    type='button'
                    fullWidth
                    variant='contained'
                    color='primary'
                    className={classes.submit}
                    onClick={ownHandleSubmit}
                    disabled={
                        hasError || loading || !inputs.email || !inputs.password
                    }
                >
                    {messages.privateRoomForm.form.BUTTON}
                </Button>
                <Button
                    type='button'
                    fullWidth
                    variant='text'
                    color='inherit'
                    className={classes.submit}
                    onClick={handleClose}
                    disabled={loading}
                >
                    {messages.privateRoomForm.form.CLOSE_BUTTON}
                </Button>
                {hasError && (
                    <Typography className={classes.error}>
                        {messages.privateRoomForm.form.ERROR_MESSAGE}
                    </Typography>
                )}
            </form>
        </Paper>
    );
};

PrivateRoomForm.propTypes = {
    cleanError: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    hasError: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
};

export default PrivateRoomForm;
