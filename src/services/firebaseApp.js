import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/functions';
import 'firebase/storage';
import 'firebase/firestore';
import { v4 as uuidv4 } from 'uuid';

const config = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
};

const createFirebaseApp = () => {
    app.initializeApp(config);

    const auth = app.auth();
    const functions = app.functions();
    const storage = app.storage();
    const firestore = app.firestore();

    const collections = {
        images: 'images',
        privateRoom: 'privateRooms',
    };

    const functionsNames = {
        createImage: 'addImage',
        createPrivateRoom: 'createPrivateRoom',
        createUser: 'createUser',
        deleteImage: 'deleteImage',
        getCategories: 'getCategories',
        getImages: 'getImages',
        getPrivateRoomByOwnerId: 'getPrivateRoomByOwnerId',
        getPrivateRooms: 'getPrivateRooms',
    };

    const createImage = async (category, image, privateRoomCode) => {
        if (!image.type.startsWith('image/')) {
            throw new Error('Invalid file type');
        }

        const path = `${collections.images}/${uuidv4()}`;
        const storageRef = storage.ref(path);
        const metadata = {
            contentType: image.type,
        };

        await storageRef.put(image, metadata);

        try {
            const url = await storageRef.getDownloadURL();

            await functions.httpsCallable(functionsNames.createImage)({
                category,
                image: url,
                imagePath: path,
                privateRoomCode,
            });
        } catch (err) {
            await storageRef.delete();

            throw err;
        }
    };

    const createPrivateRoom = async ownerId => {
        const result = await functions.httpsCallable(
            functionsNames.createPrivateRoom
        )({
            ownerId,
        });

        return result.data;
    };

    const createUser = async (email, password) => {
        const result = await functions.httpsCallable(functionsNames.createUser)(
            {
                email,
                password,
                customClaims: { privateRoomOwner: true },
            }
        );

        return result.data;
    };

    const deleteImage = async (imageId, privateRoomCode) => {
        const result = await functions.httpsCallable(
            functionsNames.deleteImage
        )({
            imageId,
            privateRoomCode,
        });

        return result.data;
    };

    const doSignInWithEmailAndPassword = (email, password) =>
        auth.signInWithEmailAndPassword(email, password);

    const doSignOut = () => auth.signOut();

    const getCategories = async () => {
        const result = await functions.httpsCallable(
            functionsNames.getCategories
        )();

        return result.data;
    };

    const getImages = async (privateRoomCode = '') => {
        const result = await functions.httpsCallable(functionsNames.getImages)({
            privateRoomCode,
        });

        return result.data;
    };

    const getPrivateRoomByOwnerId = async ownerId => {
        const result = await functions.httpsCallable(
            functionsNames.getPrivateRoomByOwnerId
        )({ ownerId });

        return result.data;
    };

    const getPrivateRooms = async (withImages = false) => {
        const result = await functions.httpsCallable(
            functionsNames.getPrivateRooms
        )({
            withImages,
        });

        return result.data;
    };

    const listenerImages = (fn, privateRoomCode = '') => {
        let path = '';

        if (privateRoomCode) {
            path = `${collections.privateRoom}/${privateRoomCode}/`;
        }

        path += collections.images;

        return firestore
            .collection(path)
            .orderBy('creationDate')
            .onSnapshot(snapshot => {
                const images = snapshot.docs.map(d => ({
                    id: d.id,
                    ...d.data(),
                }));

                fn(images);
            });
    };

    const listenerPrivateRooms = fn => {
        return firestore
            .collection(collections.privateRoom)
            .onSnapshot(snapshot => {
                const privateRooms = snapshot.docs.map(d => ({
                    code: d.id,
                    ...d.data(),
                }));

                fn(privateRooms);
            });
    };

    const onAuthStateChanged = fn => {
        auth.onAuthStateChanged(async user => {
            let token = '';

            if (user) {
                token = await user.getIdTokenResult();
            }

            fn(user, token?.claims);
        });
    };

    return {
        createImage,
        createPrivateRoom,
        createUser,
        deleteImage,
        doSignInWithEmailAndPassword,
        doSignOut,
        getCategories,
        getImages,
        getPrivateRoomByOwnerId,
        getPrivateRooms,
        listenerImages,
        listenerPrivateRooms,
        onAuthStateChanged,
    };
};

export default createFirebaseApp;
