import createFirebaseApp from './firebaseApp';

const api = createFirebaseApp();

export default {
    api,
};
