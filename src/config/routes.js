import constants from './constants';

const routes = {
    sign_in: {
        ROUTE: '/signin',
        AUTHORIZED_ROLES: [],
    },
    unauthorized: {
        ROUTE: '/unauthorized',
        AUTHORIZED_ROLES: [],
    },
    private_rooms: {
        ROUTE: '/privaterooms',
        AUTHORIZED_ROLES: [constants.roles.ADMIN],
    },
    images_upload: {
        ROUTE: '/imagesupload',
        AUTHORIZED_ROLES: [
            constants.roles.ADMIN,
            constants.roles.PRIVATE_ROOM_OWNER,
        ],
    },
};

export default routes;
