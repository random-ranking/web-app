const constants = {
    snackbar: {
        MAX_SNACK: 3,
        ANCHOR_ORIGIN_VERTICAL: 'bottom',
        ANCHOR_ORIGIN_HORIZONTAL: 'center',
    },
    roles: {
        ADMIN: 'admin',
        PRIVATE_ROOM_OWNER: 'privateRoomOwner',
    },
};

export default constants;
