const messages = {
    imageForm: {
        form: {
            BUTTON: 'Crear',
            CATEGORY_LABEL: 'Categoría',
            CLOSE_BUTTON: 'Cancelar',
            ERROR_MESSAGE: 'Datos inválidos',
            IMAGE_LABEL: 'Imagen',
        },
        TITLE: 'Nueva imagen',
    },
    imagesUploadPage: {
        ARIA_LABEL_NEW_IMAGE: 'Nueva imagen',
        BUTTON: 'Subir imagen',
        card: {
            DELETE_BUTTON: 'Borrar',
        },
        illustration: {
            ALT: 'Not found images',
            MESSAGE: 'Aún no cargaste imágenes',
        },
        notifications: {
            SUCCESS: 'Nueva imagen subida',
            DELETE: 'Imagen borrada',
        },
        TITLE: 'Imágenes',
        TITLE_CLIENT: 'Bienvenido a Random Ranking',
    },
    privateRoomForm: {
        form: {
            BUTTON: 'Crear',
            CLOSE_BUTTON: 'Cancelar',
            EMAIL_LABEL: 'Email del nuevo usuario',
            ERROR_MESSAGE: 'Datos inválidos',
            PASSWORD_LABEL: 'Contraseña del nuevo usuario',
        },
        TITLE: 'Nueva sala privada',
    },
    privateRoomsPage: {
        ARIA_LABEL_NEW_PRIVATE_ROOM: 'Nueva sala privada',
        BUTTON: 'Crear sala privada',
        card: {
            CODE_LABEL: 'Código',
            UID_LABEL: 'Usuario',
            EMAIL_LABEL: 'Email',
        },
        illustration: {
            ALT: 'Not found private rooms',
            MESSAGE: 'Aún no cargaste salas privadas',
        },
        notifications: {
            SUCCESS: 'Nueva sala privada',
        },
        TITLE: 'Salas privadas',
    },
    unauthorizedPage: {
        illustration: {
            ALT: 'Unauthorized',
            MESSAGE: 'Usuario no autorizado',
        },
    },
    routes: {
        IMAGES_UPLOAD: 'Imágenes',
        LOG_OUT: 'Cerrar sesión',
        PRIVATE_ROOMS: 'Salas privadas',
    },
    signIn: {
        form: {
            BUTTON: 'Iniciar Sesión',
            EMAIL_ADDRESS_LABEL: 'Correo electrónico',
            ERROR_MESSAGE: 'Correo electrónico y/o contraseña incorrectos',
            PASSWORD_LABEL: 'Contraseña',
        },
        TITLE: 'Random Ranking',
    },
    main: {
        PRIVATE_ROOM_CODE: 'Código',
    },
};

export default messages;
