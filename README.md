# Random Rankig Web

A web application to create private rooms and upload images to Random Ranking application.

### Available Scripts

In the project directory, you can run:

* `npm start`: Runs the app in the development mode.
* `npm run build`: Builds the app for production to the `build` folder.
* `npm run lint`: Run lint rules on files
* `npm run pretty-quick`: Runs Prettier on files (verbose).